function gca_prd_parseXML(txt) {
    var factory = Packages.javax.xml.parsers.DocumentBuilderFactory.newInstance();
    var builder = factory.newDocumentBuilder();
    var strinput = new Packages.java.io.StringReader(txt);
    var inputsource = new Packages.org.xml.sax.InputSource(strinput);

    return builder.parse(inputsource);
}

function gca_prd_processMapXML(mapQueryName) {
    var data, nodes, vals, curVal;
    var retArray = new Packages.java.util.ArrayList();
    try {
        var val = new Packages.java.util.ArrayList(IDVault.globalQuery(mapQueryName, {}));

        for (var i = 0, length = val.size(); i < length; i++) {
            data = IDVault.get(val.get(i), 'Map', 'DirXML-Data');
            data = gca_prd_parseXML(data);
            data.getDocumentElement().normalize();
            nodes = data.getElementsByTagName("row");

            for (var j = 0, nodeLength = nodes.getLength(); j < nodeLength; j++) {
                vals = nodes.item(j).getElementsByTagName("col");
                curVal = vals.item(0).getTextContent();
                retArray.add(curVal)
            }
        }
    } catch (e) { return retArray; }
    return retArray;
}

function gca_prd_preflow_populateMainFacilities() {
    var facArray = new Packages.java.util.ArrayList();
    try {
        facArray = IDVault.globalQuery("CHSMainFacilities", {});
    } catch (e) {
        return new Packages.java.util.ArrayList();
    }

    return facArray;
}
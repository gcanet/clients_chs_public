function gcaPRDForm(form, IDVault) {
    this.form = form;
    this.vault = IDVault;
}
gcaPRDForm.prototype.vault_get = function(field, obj, objClass, returnVal) {
    return this.vault.get(field, obj, objClass, returnVal);
}
gcaPRDForm.prototype.vault_query = function (field, name, params) {
    return this.vault.globalQuery(field, name, params);
} 
gcaPRDForm.prototype.form_getValue = function(name) {
    return this.form.getValue(name);
}

gcaPRDForm.prototype.form_getAllValues = function(name) {
    return this.form.getAllValues(name);
}

var gcaForm;

function gca_prd_form_init(form, IDVault) {
    this.gcaForm = new gcaPRDForm(form, IDVault);
}

function gca_prd_form_selectUser_validateString(name) {
    var str = document.getElementById(name).value;
    if (str == null || str == '') { str = "*"; }
    return str;
}

function gca_prd_form_selectUser_validateFacility(name) {
    var fac = document.getElementById(name).value;
    if (fac == '.Select a hospital.' || fac == null) {
        return "INVALID";
    } else {
        return this.gcaForm.vault_get(null, fac, "CHSFacility", "CHS-FacilityCode");
    }
}

function gca_prd_form_selectUser_getUsers(field, document, section) {
	var fac = gca_prd_form_selectUser_validateFacility(section+"_fac");
    if (fac == "INVALID") { field.setValues(''); return; }
    var fn = gca_prd_form_selectUser_validateString(section+"_fname");
    var ln = gca_prd_form_selectUser_validateString(section+"_lname");
    var un = gca_prd_form_selectUser_validateString(section+"_uname");
    var email = gca_prd_form_selectUser_validateString(section+"_email");
    var posCN = "*";

    if ((fn != "*" || ln != "*" || un != "*" || email != "*" || posCN != "*") && email != "*") {
        this.gcaForm.vault_query(section+"_selectedUser", "CHSGSuiteHospitalUsers", { "fac": fac, "fn": fn, "ln": ln, "un": un, "email": email, "pos": posCN });
    } else if ((fn != "*" || ln != "*" || un != "*" || email != "*" || posCN != "*") && email == "*") {
        this.gcaForm.vault_query(section+"_selectedUser", "HospitalUsers", { "fac": fac, "fn": fn, "ln": ln, "un": un });
    } else { field.setValues(''); }
}

function gca_prd_form_selectUser_searchUsers(section) {
	var html = "<hr><h4>Use the following fields to search for a user:</h4>"
	html += "<p><span style=\"margin-left: 35%;margin-bottom 10px;\"><label for=\"fac\">Facility:    </label>"
	html += "<select name=\"fac\" id=\""+section+"_fac\"><option value=\".Select a hospital.\">.Select a hospital.</option></select></span><br><br>"
	html += "<span style=\"margin-left: 20%;\"><label for=\"fname\">First Name:  </label>"
	html += "<input type=\"text\" id=\""+section+"_fname\" name=\"fname\"></span>"
	html += "<span style=\"padding-left: 20px;\"><label for=\"lname\">Last Name:  </label>"
	html += "<input type=\"text\" id=\""+section+"_lname\" name=\"lname\"></span><br><br>"
	html += "<span style=\"margin-left: 20%;\"><label for=\"fname\">User Name:  </label>"
	html += "<input type=\"text\" id=\""+section+"_uname\" name=\"uname\"></span>"
	html += "<span style=\"padding-left: 20px;\"><label for=\"lname\">Email:  </span></label>"
	html += "<span style=\"padding-left: 30px;\"><input type=\"text\" id=\""+section+"_email\" name=\"email\"></span><br><br></p>"

	html += "<p span style=\"margin-left: 45%;padding-bottom 10px\;\"><input type=\"button\" value=\"Search Users\" onClick=\"var ctrl = JUICE.UICtrlUtil.getControl(&quot;"+section+"_selectedUser&quot;); var vals = ctrl.getValues(); var evt = new JUICE.WFASEvent(&quot;"+section+"_userSearch&quot;,&quot;"+section+"_getUsers&quot;, vals, null, null);  var evt_array = new Array(evt); dojo.publish(&quot;WFAS_"+section+"_getUsers&quot;, evt_array); \"></p>"
    return html;
}

function gca_prd_form_selectUser_fillFacilities(section) {
	var facs = this.gcaForm.form_getAllValues("internal_facilities");
	var fac = document.getElementById(section+"_fac");
	for(i = 0; i<facs[0].length; i++ ) {
		var curFac = facs[0][i];
		var option = document.createElement("option");
		option.value = curFac;
		var curFacName = curFac.split(',ou')[0].substring(3);
		option.text = curFacName;
		fac.add(option);
	}
}